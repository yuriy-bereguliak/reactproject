import {StyleSheet} from 'react-native';

export default StyleSheet.create({

  container:{
    flex:1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
    justifyContent: 'center'
  },

  title: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#66BB6A',
  },

  description: {
    fontSize: 18,
    color: '#757575',
  },

  date: {
    fontSize: 18,
    color: '#757575',
  },

  phone: {
    fontSize: 16,
    marginTop: 20,
    color: '#0000FF',
  },
});
