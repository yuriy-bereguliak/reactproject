import {StyleSheet} from 'react-native';

export default StyleSheet.create({

  noRouteContainer:{flex: 1, alignItems: 'stretch', justifyContent: 'center'},
  noRouteOpacity:{flex: 1, alignItems: 'center', justifyContent: 'center'},
  noRouteText:{color: 'red', fontWeight: 'bold'},
});
