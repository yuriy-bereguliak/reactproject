import {StyleSheet} from 'react-native';

export default StyleSheet.create({

  loadingText:{
    fontSize: 12,
    color: '#757575',
    alignItems: 'center',
  },

});
