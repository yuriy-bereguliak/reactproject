import {StyleSheet} from 'react-native';

export default StyleSheet.create({

  container:{
    flex:1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    backgroundColor: '#246dd5',
    alignItems: 'center',
    justifyContent: 'center'
  },

  loadingText:{
    color: 'white',
    fontSize: 16,
  },
});
