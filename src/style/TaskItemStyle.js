import {
    StyleSheet
} from 'react-native';

export default StyleSheet.create( {
    separator: {
        flex: 1,
        height: StyleSheet.hairlineWidth,
        backgroundColor: '#8E8E8E',
        marginLeft: 16,
    },

    container : {
      paddingBottom: 8,
      paddingTop: 8,
      paddingLeft: 16,
    },

    title: {
      fontSize: 18,
      fontWeight: 'bold',
      color: '#66BB6A',
    },

    description: {
      color: '#757575',
    },

    date: {
      color: '#757575',
    }
  }
);
