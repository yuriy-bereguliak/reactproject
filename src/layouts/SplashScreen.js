import React, {
    Component
} from 'react';
import {
    View,
    Text,
} from 'react-native';

import styles from '../style/SplashScreenStyle.js';

class SplashScreen extends Component {

    componentWillMount() {
        var navigator = this.props.navigator;
        setTimeout(() => {
            navigator.replace({
                id: 'MainScreen',
            });
        }, 1000);
    }

    render() {
        return ( <View style={styles.container}>
            <Text style={styles.loadingText}>Loading...</Text>
            </View>
        );
    }
}

module.exports = SplashScreen;
