import React, {
    Component
} from 'react';
import {
    View,
    Text,
    ListView,
    TouchableHighlight,
    ToastAndroid,
    BackAndroid,
} from 'react-native';

import taskItemViewStyle from '../style/TaskItemStyle.js';
import styles from '../style/MainScreenStyle.js';

import api from '../utils/api.js';

class MainScreen extends Component {

    constructor(props) {
        super(props);
        var navigator = this.props.navigator;

        this.state = {
          dataSource: new ListView.DataSource({
            rowHasChanged: (row1, row2) => row1 !== row2
          }),
          loaded: false
        };
    }

    componentDidMount(){
      api.getListItems().then((res) => {
          this.setState({
              dataSource: this.state.dataSource.cloneWithRows(res),
              loaded:true
          })
      });
    }

    render() {
        if (!this.state.loaded) {
          return this.renderLoadingView();
        }
        return (
          <ListView
            dataSource={this.state.dataSource}
            renderRow={this.renderTaskView.bind(this)}
            renderSeparator={(sectionId, rowId) => <View key={rowId} style={taskItemViewStyle.separator} />}
          />
        );
    }

    renderLoadingView(){
      return (
        <View>
          <Text style={styles.loadingText}>Task Loading ....</Text>
        </View>
      );
    }

    renderTaskView(task){
      return(
        <View style={taskItemViewStyle.container}>
          <TouchableHighlight
           onPress={() => this.showMessage(task)}
           underlayColor='#E0E0E0'>
            <View>
              <Text style={taskItemViewStyle.title}>{task.taskTitle}</Text>
              <Text style={taskItemViewStyle.description}>{task.taskDescription}</Text>
              <Text style={taskItemViewStyle.date}>{task.taskCreate}</Text>
            </View>
          </TouchableHighlight>
        </View>
      );
    }

    showMessage(task){
      this.props.navigator.push({
          id: 'DetailsScreen',
          passProps: {
            taskProp: task
          }
      });
    }
}

module.exports = MainScreen;
