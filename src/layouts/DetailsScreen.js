import React, {
    Component
} from 'react';
import {
    View,
    Text,
    BackAndroid,
    TouchableOpacity,
} from 'react-native';

import Communications from 'react-native-communications';

import styles from '../style/DetailsScreenStyle.js';

class DetailsScreen extends Component {

    constructor(props){
      super(props);

      var navigator = this.props.navigator;

      BackAndroid.addEventListener('hardwareBackPress', function() {
        if (navigator.getCurrentRoutes().length === 1) {
          return false;
        } else {
          navigator.pop();
          return true;
        }
        return true;
      });
    }

    render() {
        return ( <View style={styles.container}>
            <Text style={styles.title}>{this.props.taskProp.taskTitle}</Text>
            <Text style={styles.description}>{this.props.taskProp.taskDescription}</Text>
            <Text style={styles.date}>{this.props.taskProp.taskCreate}</Text>

            <TouchableOpacity
              onPress={() => Communications.phonecall('0939155645', true)}>
              <Text style={styles.phone}>0939155645</Text>
            </TouchableOpacity>
            </View>
        );
    }
}

module.exports = DetailsScreen;
