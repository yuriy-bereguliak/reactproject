
var api = {
    getListItems() {
        return fetch('https://moneycache.herokuapp.com/api/v1/status/json').then(parseJson);
    }
};

var parseJson = function(response) {
    return response.json();
}

module.exports = api;
