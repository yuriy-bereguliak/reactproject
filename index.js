import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  TextInput,
  TouchableHighlight,
  Button,
  Navigator,
  View
} from 'react-native';

import styles from './src/style/IndexAndroidStyle.js';

var SplashScreen = require('./src/layouts/SplashScreen.js');
var MainScreen = require('./src/layouts/MainScreen.js');
var DetailsScreen = require('./src/layouts/DetailsScreen.js');

export default class ReactProject extends Component {

  render() {
    return (
      <Navigator
        initialRoute={{id:'SplashScreen', name: 'Index'}}
        renderScene={this.renderScene.bind(this)}
        configureScene={(route) => {
          if (route.sceneConfig) {
            return route.sceneConfig;
          }
          return Navigator.SceneConfigs.FloatFromRight;
        }}
        />
    );
  }

  renderScene(route, navigator) {
    var routeId = route.id;
    if (routeId === 'SplashScreen') {
      return (
        <SplashScreen
          navigator={navigator} />
      );
    }
    if (routeId === 'MainScreen') {
      return (
        <MainScreen
          navigator={navigator} />
      );
    }
    if (routeId === 'DetailsScreen') {
      return (
        <DetailsScreen
          navigator={navigator}{...route.passProps} />
      );
    }
  }

  noRoute(navigator) {
    return (
      <View style={styles.noRouteContainer}>
        <TouchableOpacity style={styles.noRouteOpacity}
            onPress={() => navigator.pop()}>
          <Text style={styles.noRouteText}>Render error</Text>
        </TouchableOpacity>
      </View>
    );
  }

}
