import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  TextInput,
  TouchableHighlight,
  Button,
  Navigator,
  View
} from 'react-native';

import ReactProject from './index.js';

AppRegistry.registerComponent('ReactProject', () => ReactProject);
